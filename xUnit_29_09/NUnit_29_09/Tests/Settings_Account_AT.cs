﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace NUnit_29_09
{
    class Settings_Account_AT
    {
        //Autorization autorization = new Autorization();
        Settings settings = new Settings();

        [SetUp]
        public void ClassName()
        {
            settings.autorization.OpenCrome();
        }

        [Theory]
        [TestCase("nasjo@gmail.com", "n49bpxeCYG$A!Lb")]
        public void CheckSettingsAccount(string name, string password)
        {
            settings.autorization.ClickAndWriteMail(name);
            settings.autorization.ClickAndWritePassord(password);
            settings.autorization.ClickLogIn();
            //click on the account image
            settings.ClickAccount();            
            settings.ClickPen();

            Thread.Sleep(1000);
            Assert.AreEqual("https://newbookmodels.com/account-settings/account-info/edit", settings.autorization.driver.Url);
        }

        [TearDown]
        public void Teardown()
        {
            settings.autorization.driver.Quit();
        }
    }
}
