﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects
{
    class Registration2PageObject
    {
        private IWebDriver _webDriver;
        private readonly By _companyName = By.CssSelector(" input[name='company_name']");
        private readonly By _companySite = By.Name("company_website");
        private readonly By _location = By.Name("location");
        private readonly By _listLocation = By.Name("_hjRemoteVarsFrame");
        private readonly By _selectIndustry = By.XPath("//input[@class='Select__valueBox--2VlHF']");
        private readonly By _cosmaticsIndustry = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[3]");
        private readonly By _otherIndustry = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[7]");
        private readonly By _inputIndustry = By.CssSelector("input[name=industry_other]");
        private readonly By _buttonFinish = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/div/button");

       
        private readonly By _companyNameError = By.CssSelector("input[name='company_name']+div[class^='FormErrorText'] > div");
        private readonly By _companySiteError = By.CssSelector("input[name='company_website']+div[class^='FormErrorText'] > div");
        private readonly By _adressError = By.CssSelector("input[name='location']+div[class^='FormErrorText'] > div");
        private readonly By _industryError = By.CssSelector("input[name='industry_other']+div[class^='FormErrorText'] > div");
        
        public Registration2PageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        IWebElement GetCompanyName() => _webDriver.FindElement(_companyName);
        IWebElement GetCompanyWebSite() => _webDriver.FindElement(_companySite);
        IWebElement GetCompanyLocation() => _webDriver.FindElement(_location);
        IWebElement GetMapsLocation() => _webDriver.FindElement(_listLocation);
        IWebElement GetIndustry() => _webDriver.FindElement(_selectIndustry);
        IWebElement GetCosmaticsIndustry() => _webDriver.FindElement(_cosmaticsIndustry);
        IWebElement GetOtherIndustry() => _webDriver.FindElement(_otherIndustry);
        IWebElement GetInputIndustry() => _webDriver.FindElement(_inputIndustry);
        public IWebElement GetButtonFinish() => _webDriver.FindElement(_buttonFinish);
        public string GetUrl() => _webDriver.Url;
        public string GetErrorCompanyName() => _webDriver.FindElement(_companyNameError).Text;
        public string GetErrorCompanySiteError() => _webDriver.FindElement(_companySiteError).Text;
        public string GetErrorAdress() => _webDriver.FindElement(_adressError).Text;
        public string GetErrorIndustry() => _webDriver.FindElement(_industryError).Text;
        public Registration1PageObject Registration(string companyName, string companySite, string adress)
        {
            var FieldCompanyName = GetCompanyName();
            FieldCompanyName.Click();
            FieldCompanyName.SendKeys(companyName);
            FieldCompanyName.SendKeys(Keys.Enter);

            var FieldWebSite = GetCompanyWebSite();
            FieldWebSite.Click();
            FieldWebSite.SendKeys(companySite);
            FieldWebSite.SendKeys(Keys.Enter);

            var FieldLocation = GetCompanyLocation();
            FieldLocation.Click();
            FieldLocation.SendKeys(adress);
            FieldLocation.SendKeys(Keys.Up);
            Actions action = new Actions(_webDriver);
            action.MoveToElement(GetMapsLocation());
            action.KeyDown(Keys.LeftShift);

            return new Registration1PageObject(_webDriver);
        }
        public void SelectIndustry()
        {
            GetIndustry().Click();
            GetCosmaticsIndustry().Click();
        }
        public void OtherIndustry(string nameIndustry)
        {
            GetIndustry().Click();
            GetOtherIndustry().Click();
            var fieldIndustry = GetInputIndustry();
            fieldIndustry.SendKeys(nameIndustry);
            fieldIndustry.SendKeys(Keys.Enter);
        }
    }
}
