﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09
{
    class Settings
    {
        public Autorization autorization = new Autorization();
        public void ClickAccount()
        {
            IWebElement buttonNext = autorization.driver.FindElement(By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/common-header/section/nb-main-header/common-react-bridge/header/div/div/div[2]/a[3]/div/div"));
            buttonNext.Click();
        }
        public void ClickPen()
        {
            IWebElement buttonPen = autorization.driver.FindElement(By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[1]/div/nb-account-info-general-information/form/div[1]/div/nb-edit-switcher/div/div"));
            buttonPen.Click();
        }
        public void ClickSave()
        {
            IWebElement buttonPen = autorization.driver.FindElement(By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[1]/div/nb-account-info-general-information/form/div[2]/div/common-button-deprecated/button"));
            buttonPen.Click();
        }
    }
}
