﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace NUnit_29_09
{
    public class Registration
    {
        public IWebDriver driver;
        //IJavaScriptExecutor js;
        //js = (IJavaScriptExecutor)driver;
        //string title = (string)js.ExecuteScript("return document.title");
        public void OpenCrome()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        public string ClickOn_SignUp()
        {
            IWebElement search = driver.FindElement(By.XPath("/html/body/nb-app/nb-home-page/common-react-bridge/div/header/span[1]/button"));
            search.Click();
            return driver.Url.ToString();
        }
        public void ClickAndWriteDownFirstName(string name)
        {            
            IWebElement firstName = driver.FindElement(By.Name("first_name"));
            firstName.Click();
            firstName.SendKeys(name);
            firstName.SendKeys(Keys.Enter);
        }
        public void ClickAndWriteDownLastName(string lastname)
        {
            IWebElement lastName = driver.FindElement(By.Name("last_name"));
            lastName.Click();
            lastName.SendKeys(lastname);
            lastName.SendKeys(Keys.Enter);
        }
        public void ClickAndWriteMail(string email)
        {
            IWebElement mail = driver.FindElement(By.Name("email"));
            mail.Click();
            mail.SendKeys(email);
            mail.SendKeys(Keys.Enter);
        }
        public void ClickAndWritePassword(string password)
        {
            IWebElement pass = driver.FindElement(By.Name("password"));
            pass.Click();
            pass.SendKeys(password);
            pass.SendKeys(Keys.Enter);
        }
        public void ClickAndWriteConfirmPassword(string configPassword)
        {
            IWebElement confPass = driver.FindElement(By.Name("password_confirm"));
            confPass.Click();
            confPass.SendKeys(configPassword);
            confPass.SendKeys(Keys.Enter);
        }
        public void ClickAndWritePhone(string phoneNum)
        {
            IWebElement phone = driver.FindElement(By.Name("phone_number"));
            phone.Click();
            phone.SendKeys(phoneNum);
            phone.SendKeys(Keys.Enter);
        }
        public void ClickNext()
        {
            IWebElement buttonNext = driver.FindElement(By.XPath("/html/body/nb-app/nb-signup/common-react-bridge/div/div[2]/div/section/section/div[1]/form/section/section/div/div[2]/section/div/section/button"));
            buttonNext.Click();
        }
        public void ClickAndWriteCompanyName(string nameCompany)
        {
            IWebElement name = driver.FindElement(By.Name("company_name"));
            name.Click();
            name.SendKeys(nameCompany);
            name.SendKeys(Keys.Enter);
        }
        public void ClickAndWriteCompanyWeb(string companySite)
        {
            IWebElement companyWeb = driver.FindElement(By.Name("company_website"));
            companyWeb.Click();
            companyWeb.SendKeys(companySite);
            companyWeb.SendKeys(Keys.Enter);
        }
        public void ClickAndSelectCompanyLocation(string name)
        {
            IWebElement location = driver.FindElement(By.Name("location"));
            IWebElement location2 = driver.FindElement(By.Name("_hjRemoteVarsFrame"));

            location.SendKeys(name);
            location.SendKeys(Keys.Up);
            Actions action = new Actions(driver);
            action.MoveToElement(location2);
            action.KeyDown(Keys.LeftShift);
        }
        public void ClickAndSelectIndustry()
        {
            IWebElement industry = driver.FindElement(By.XPath("//input[@class='Select__valueBox--2VlHF']"));
            industry.Click();

            IWebElement industry2 = driver.FindElement(By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[3]"));
            industry2.Click();
        }
        public void ClickAndSelectIndustryOther(string name)
        {
            IWebElement industry = driver.FindElement(By.XPath("//input[@class='Select__valueBox--2VlHF']"));
            industry.Click();

            IWebElement industry2 = driver.FindElement(By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[7]"));
            industry2.Click();

            IWebElement industry3 = driver.FindElement(By.CssSelector("input[name=industry_other]"));
            industry3.SendKeys(name);
            industry3.SendKeys(Keys.Enter);
        }
        public void ClickFinish()
        {
            IWebElement buttonFinish = driver.FindElement(By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/div/button"));
            buttonFinish.Click(); 
        }
    }
}
