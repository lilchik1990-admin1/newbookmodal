﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace NUnit_29_09.PageObjects.Settings.AccountInfo
{
    class AddCardObjectModel
    {
        private IWebDriver _webDriver;
        private readonly By _settings = By.CssSelector("a[href='/account-settings'] > div > div");
        private readonly By _fullName = By.CssSelector("common-input[placeholder='Full name']>label>input");
        public AddCardObjectModel(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public IWebElement GetSetting() => _webDriver.FindElement(_settings);
        IWebElement GetFullNameForCart() => _webDriver.FindElement(_fullName);
    }
}
