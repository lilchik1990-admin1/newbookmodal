﻿using NUnit.Framework;
using NUnit_29_09.PageObjects.AutorizationPO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using NUnit_29_09.PageObjects.Settings;
using NUnit_29_09.PageObjects.Settings.AccountInfo;

namespace NUnit_29_09.Tests
{
    class SettingsAccountTests
    {
        public IWebDriver driver;
        //public Autorization autorization = new Autorization();
        [SetUp]
        public void ClassName()//https://newbookmodels.com/explore
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/auth/signin");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            AutorizationPageObject autorization = new AutorizationPageObject(driver);
            autorization.Autorization("nasjo@gmail.com", "n49bpxeCYG$A!Lb");
            autorization.GetButtonLogIn().Click();
        }
        [Theory]
        [TestCase("Lilia", "yandov2", "","")]
        public void CheckGeneralInformation(string firstName, string lastName, string locationName, string industry)
        {
            GeneralInformationPageObject generalInformation = new GeneralInformationPageObject(driver);
            generalInformation.GetSetting().Click();
            generalInformation.GetPencil().Click();
            generalInformation.GeneralInformation(firstName, lastName, locationName, industry);
            var button = generalInformation.GetSaveChange();
            //Thread.Sleep(1000);
            //button.Click();//почему-то не кликается....;(

         }
        [Theory]
        [TestCase("Uldms223kdmOPO38", "yandov2@hsla.com")]
        public void CheckEmailPage(string password, string email)
        {
            EmailPageObject emailPage = new EmailPageObject(driver);
            emailPage.GetSetting().Click();
            emailPage.GetPencil().Click();
            emailPage.EmailPage(password, email);
            emailPage.GetSaveChange().Click();
        }
        [Theory]
        [TestCase("Uldms223kdmOPO38", "Uldms223kdmOHKJPO38", "Uldms223kdmOHKJPO38")]
        public void CheckPasswordModule(string password, string newPassword, string newPassword2)
        {
            PasswordObjectModel passwordObject = new PasswordObjectModel(driver);
            var close = passwordObject.GetClose();
            close.Click();
            passwordObject.GetSetting().Click();
            passwordObject.GetPencil().Click();
            passwordObject.PasswordModule(password, newPassword, newPassword2);            
           //passwordObject.GetSaveChange().Click();//почему-то не кликабельна
        }
        [Theory]
        [TestCase("Uldms223kdmOPO38", "7708878374")]
        public void CheckPhoneModule(string password, string phone)
        {
            PhoneObjectModel phoneObject = new PhoneObjectModel(driver);
            phoneObject.GetClose().Click();
            phoneObject.GetSetting().Click();
            phoneObject.GetPencilPhone().Click();
            phoneObject.PhoneModule(password, phone);
            phoneObject.GetSaveChange().Click();
        }
        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
