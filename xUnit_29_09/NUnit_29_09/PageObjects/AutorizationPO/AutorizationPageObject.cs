﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects.AutorizationPO
{
    public class AutorizationPageObject
    {
        private IWebDriver _webDriver;
        private readonly By _login = By.Name("email");
        private readonly By _password = By.Name("password");
        private readonly By _buttonLogIn = By.XPath("/html/body/nb-app/ng-component/common-react-bridge/div/div[2]/div/form/section/section/div[3]/button");

        private readonly By _loginError = By.CssSelector("input[name='email']+div[class^='FormErrorText'] > div");
        private readonly By _passwordError = By.CssSelector("input[name='password']+div[class^='FormErrorText'] > div");

        public AutorizationPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        IWebElement GetLogin() => _webDriver.FindElement(_login);
        IWebElement GetPassword() => _webDriver.FindElement(_password);
        public IWebElement GetButtonLogIn() => _webDriver.FindElement(_buttonLogIn);
        public string GetUrl() => _webDriver.Url;
        public string GetErrorLogin() => _webDriver.FindElement(_loginError).Text;
        public string GetErrorPassword() => _webDriver.FindElement(_passwordError).Text;

        public AutorizationPageObject Autorization(string name, string password)
        {
            var FieldLogin = GetLogin();
            FieldLogin.Click();
            FieldLogin.SendKeys(name);
            FieldLogin.SendKeys(Keys.Enter);

            var FieldPassword = GetPassword();
            FieldPassword.Click();
            FieldPassword.SendKeys(password);
            FieldPassword.SendKeys(Keys.Enter);

            return new AutorizationPageObject(_webDriver);
        }
    }
}
