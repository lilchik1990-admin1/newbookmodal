﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects.Settings.AccountInfo
{
    class PhoneObjectModel
    {
        private IWebDriver _webDriver;

        private readonly By _settings = By.CssSelector("a[href='/account-settings'] > div > div");
        private readonly By _pencilPhone = By.CssSelector("nb-account-info-phone>div>div>nb-edit-switcher>div>div");
        private readonly By _modalClose = By.CssSelector("#resend-email > div > div.modal.d-flex.justify-content-center > div > div.modal__close");
        private readonly By _password3 = By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[5]/div/nb-account-info-phone/div[2]/div/form/common-input/label/input");
        private readonly By _newPhoneNumber3 = By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[5]/div/nb-account-info-phone/div[2]/div/form/common-input-phone/label/input");
        private readonly By _saveChange3 = By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[5]/div/nb-account-info-phone/div[2]/div/form/common-button-deprecated/button");
        public PhoneObjectModel(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
       public  IWebElement GetSetting() => _webDriver.FindElement(_settings);
        public IWebElement GetPencilPhone() => _webDriver.FindElement(_pencilPhone);
        public IWebElement GetClose() => _webDriver.FindElement(_modalClose);
        IWebElement GetPasswordFromPhoneNum() => _webDriver.FindElement(_password3);
        IWebElement GetPhoneFromPhoneNumber() => _webDriver.FindElement(_newPhoneNumber3);
        public IWebElement GetSaveChange() => _webDriver.FindElement(_saveChange3);
        public PhoneObjectModel PhoneModule(string password, string phone)
        {
            if (!password.Equals(""))
            {
                var pass = GetPasswordFromPhoneNum();
                pass.Click();
                pass.Clear();
                pass.SendKeys(password);
                pass.SendKeys(Keys.Enter);
            }
            if (!phone.Equals(""))
            {
                var phoneNum = GetPhoneFromPhoneNumber();
                phoneNum.Click();
                phoneNum.Clear();
                phoneNum.SendKeys(password);
                phoneNum.SendKeys(Keys.Enter);
            }

            return new PhoneObjectModel(_webDriver);
        }

    }
}
