﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects.Settings.AccountInfo
{
    public class GeneralInformationPageObject
    {
        private IWebDriver _webDriver;
        private readonly By _settings = By.CssSelector("a[href='/account-settings'] > div > div");

        private readonly By _pencil = By.CssSelector("nb-account-info-general-information>form>div>div>nb-edit-switcher>div>div");
        private readonly By _firstName = By.XPath("//common-input[@formcontrolname = 'first_name']//input");
        private readonly By _lastName = By.XPath("//common-input[@formcontrolname = 'last_name']//input");
        private readonly By _companyLocation = By.XPath("//input[@class = 'input__self input__self_type_text-underline ng-valid ng-touched ng-dirty']");
        private readonly By _elemFromIframe = By.Name("__privateStripeMetricsController3120");
        private readonly By _industry = By.XPath("//common-input[@formcontrolname = 'industry']//input");
        private readonly By _buttonSaveChange = By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/section/div/ng-component/nb-account-info-edit/common-border/div[1]/div/nb-account-info-general-information/form/div[2]/div/common-button-deprecated/button");

        public GeneralInformationPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public IWebElement GetSetting() => _webDriver.FindElement(_settings);
        public IWebElement GetPencil() => _webDriver.FindElement(_pencil);
        IWebElement GetFirstNamel() => _webDriver.FindElement(_firstName);
        IWebElement GetLastName() => _webDriver.FindElement(_lastName);
        IWebElement GetCompanyLocation() => _webDriver.FindElement(_companyLocation);
        IWebElement GetElemIframe() => _webDriver.FindElement(_elemFromIframe);
        IWebElement GetIndustry() => _webDriver.FindElement(_industry);
        public IWebElement GetSaveChange() => _webDriver.FindElement(_buttonSaveChange);
        public string GetUrl() => _webDriver.Url;

        public void GeneralInformation(string firstName, string lastName, string locationName, string industry)
        {
            if (!firstName.Equals(""))
            {
                var nameFirst = GetFirstNamel();
                nameFirst.Click();
                nameFirst.Clear();
                nameFirst.SendKeys(firstName);
                nameFirst.SendKeys(Keys.Enter);
            }
            var nameLast = GetLastName();
            if (!lastName.Equals(""))
            {
                nameLast.Click();
                nameLast.Clear();
                nameLast.SendKeys(lastName);
                nameLast.SendKeys(Keys.Enter);
            }
            if (!locationName.Equals(""))
            {
                var nameLocation = GetCompanyLocation();
                nameLocation.Click();
                nameLocation.SendKeys(locationName);
                nameLocation.SendKeys(Keys.Up);
                Actions action = new Actions(_webDriver);
                action.MoveToElement(GetElemIframe());
                action.KeyDown(Keys.LeftShift);
            }
            if (!industry.Equals(""))
            {
                var nameindustry = GetIndustry();
                nameindustry.Click();
                nameindustry.SendKeys(industry);
                nameindustry.SendKeys(Keys.Enter);
            }
        }
    }
}
