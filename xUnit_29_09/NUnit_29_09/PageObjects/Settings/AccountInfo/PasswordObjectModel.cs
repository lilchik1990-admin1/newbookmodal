﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects.Settings.AccountInfo
{
    class PasswordObjectModel
    {
        private IWebDriver _webDriver;

        private readonly By _settings = By.CssSelector("a[href='/account-settings'] > div > div");

        private readonly By _pencil = By.CssSelector("nb-account-info-password>form>div>div>nb-edit-switcher>div");

        private readonly By _modalClose = By.CssSelector("#resend-email > div > div.modal.d-flex.justify-content-center > div > div.modal__close");
        private readonly By _enterCurrentPassword = By.CssSelector("body > nb-app > ng-component > nb-internal-layout > common-layout > section > div > ng-component > nb-account-info-edit > common-border > div:nth-child(5) > div > nb-account-info-password > form > div:nth-child(2) > div > common-input:nth-child(2) > label > input"/*"//common-input[@formcontrolname='old_password']//input[@placeholder='Enter Current Password'"*/);
        private readonly By _enterNewPassword = By.XPath("//common-input[@formcontrolname='new_password']//input[@placeholder='Enter New Password']");
        private readonly By _EnterNewPassword2 = By.XPath("//common-input[@formcontrolname='newPasswordConfirmation']//input[@placeholder='Enter New Password']");
        private readonly By _buttonSavePassword = By.XPath("//nb-account-info-password//button[@class='button button_type_default'");
        public PasswordObjectModel(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public IWebElement GetSetting() => _webDriver.FindElement(_settings);
        public IWebElement GetPencil() => _webDriver.FindElement(_pencil);
        public IWebElement GetClose() => _webDriver.FindElement(_modalClose);
        IWebElement GetCurrentPassword() => _webDriver.FindElement(_enterCurrentPassword);
        IWebElement GetNewPassword() => _webDriver.FindElement(_enterNewPassword);
        IWebElement GetNewPassword2() => _webDriver.FindElement(_EnterNewPassword2);
        public IWebElement GetSaveChange() => _webDriver.FindElement(_buttonSavePassword);

        public PasswordObjectModel PasswordModule(string password, string newPassword, string newPassword2)
        {
            //_webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            
            //var close = GetClose();
            //close.Click();
            if (!password.Equals(""))
            {
                var pass = GetCurrentPassword(); 
                pass.Click();
                pass.Clear();
                pass.SendKeys(password);
                pass.SendKeys(Keys.Enter);
            }
            if (!newPassword.Equals(""))
            {
                var newPass = GetNewPassword();
                newPass.Click();
                newPass.Clear();
                newPass.SendKeys(password);
                newPass.SendKeys(Keys.Enter);
            }
            if (!newPassword2.Equals(""))
            {
                var newPass = GetNewPassword2();
                newPass.Click();
                newPass.Clear();
                newPass.SendKeys(password);
                newPass.SendKeys(Keys.Enter);
            }

            return new PasswordObjectModel(_webDriver);
        }
    }
}
