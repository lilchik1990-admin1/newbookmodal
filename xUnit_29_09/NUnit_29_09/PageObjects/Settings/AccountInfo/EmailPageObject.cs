﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects.Settings.AccountInfo
{
    class EmailPageObject
    {
        private IWebDriver _webDriver;
        private readonly By _settings = By.CssSelector("a[href='/account-settings'] > div > div");
        private readonly By _pencil = By.CssSelector("nb-account-info-email-address>form>div>div>nb-edit-switcher>div>div");

        private readonly By _enterCurrentPassword = By.XPath("//label[@class='input__label input__label_type_password-underline']//input");
        private readonly By _enterMail = By.XPath("//nb-account-info-email-address//input[@placeholder='Enter E-mail']");
        private readonly By _buttonSaveChange = By.XPath("//common-button-deprecated[@class='mt-5']//button");
        public EmailPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public IWebElement GetSetting() => _webDriver.FindElement(_settings);
        public IWebElement GetPencil() => _webDriver.FindElement(_pencil);
        IWebElement GetCurrentPassword() => _webDriver.FindElement(_enterCurrentPassword);
        IWebElement GetEmail() => _webDriver.FindElement(_enterMail);
        public IWebElement GetSaveChange() => _webDriver.FindElement(_buttonSaveChange);
        public EmailPageObject EmailPage(string password, string email)
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            
            if (!password.Equals(""))
            {
                var pass = GetCurrentPassword();
                pass.Click();
                pass.Clear();
                pass.SendKeys(password);
                pass.SendKeys(Keys.Enter);
            }
            if (!email.Equals(""))
            {
                var mail = GetEmail();
                mail.Click();
                mail.Clear();
                mail.SendKeys(email);
                mail.SendKeys(Keys.Enter);
            }

            return new EmailPageObject(_webDriver);
        }

    }
}
